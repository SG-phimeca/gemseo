..
   Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com

   This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit
   http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
   Commons, PO Box 1866, Mountain View, CA 94042, USA.

GEMSEO aims to create, explore and optimize multidisciplinary design problems.
It is based on various technical areas which it seeks to combine in the best possible way:
applied mathematics, visualization, data manipulation or software development.
The developers thank all the open source libraries making GEMSEO possible.

External Dependencies
---------------------

GEMSEO depends on software with compatible licenses that are listed below.

`Python <http://python.org/>`_
    Python Software License

    .. image:: /_static/dependencies/python-logo.svg
        :target: https://www.python.org/


`Fast JSON schema <https://github.com/horejsek/python-fastjsonschema>`_
    BSD 3-Clause
    (Only for Python 3, for Python 2.7, FastJsonschema is embedded in GEMSEO source code)

`Graphviz <https://github.com/xflr6/graphviz>`_
    MIT

    .. image:: /_static/dependencies/graphviz-logo.png
        :target: https://github.com/xflr6/graphviz

`H5py <https://www.h5py.org/>`_
    BSD 3-Clause

`matplotlib <https://matplotlib.org/>`_
    matplotlib License

    .. image:: /_static/dependencies/matplotlib-logo.svg
        :target: https://matplotlib.org/

`nlopt <https://github.com/stevengj/nlopt>`_
    GNU LGPL v2.1

    .. image:: /_static/dependencies/Nlopt-logo.png
        :target: https://nlopt.readthedocs.io/

`numpy <https://numpy.org/>`_
    BSD 3-Clause

    .. image:: /_static/dependencies/numpy-logo.svg
        :target: https://numpy.org/

`OpenTURNS <https://github.com/openturns/openturns>`_
    GNU LGPL v3.0

    .. image:: /_static/dependencies/openturns-logo.png
        :target: https://github.com/openturns/openturns

`Pandas <https://pandas.pydata.org/>`_
    BSD 3-Clause

    .. image:: /_static/dependencies/pandas-logo.svg
        :target: https://pandas.pydata.org/

`pathlib2 <https://github.com/mcmtroffaes/pathlib2>`_
    MIT

`pyDOE <https://github.com/tisimst/pyDOE>`_
    BSD 3-Clause

    .. image:: /_static/dependencies/pydoe-logo.png
        :target: https://github.com/tisimst/pyDOE

`pyDOE2 <https://github.com/clicumu/pyDOE2>`_
    BSD 3-Clause

    .. image:: /_static/dependencies/pydoe-logo.png
        :target: https://github.com/clicumu/pyDOE2

`PySide2 <https://wiki.qt.io/index.php?title=Qt_for_Python>`_
    GNU LGPL v3.0
    (When PySide 2 is unavailable, PyQt is used, distributed under the GNU GPL v3.0.)

`Pytest <https://pytest.org>`_
    MIT

    .. image:: /_static/dependencies/pytest-logo.svg
        :target: https://pytest.org

`Python-future <https://github.com/PythonCharmers/python-future>`_
    MIT

`pyXDSM <https://github.com/mdolab/pyXDSM>`_
    Apache v2.0

`requests <https://github.com/psf/requests>`_
    Apache v2.0

    .. image:: /_static/dependencies/requests-logo.png
        :target: https://github.com/psf/requests

`scikit-learn <https://scikit-learn.org/>`_
    BSD 3-Clause

    .. image:: /_static/dependencies/scikit-learn-logo.svg
        :target: https://scikit-learn.org/

`scipy <https://www.scipy.org/scipylib/>`_
    BSD 3-Clause

    .. image:: /_static/dependencies/scipy-logo.png
        :target: https://www.scipy.org/scipylib/


`setuptools <https://setuptools.readthedocs.io/>`_
    MIT

`snopt-python <https://github.com/snopt/snopt-python>`_
    MIT

`six <https://github.com/benjaminp/six>`_
    MIT

`SymPy <https://www.sympy.org/>`_
    BSD 3-Clause

    .. image:: /_static/dependencies/sympy-logo.svg
        :target: https://www.sympy.org/

`XDSMjs <https://github.com/OneraHub/XDSMjs>`_
    Apache v2.0

`xlwings CE <https://www.xlwings.org/>`_
    BSD 3-Clause

    .. image:: /_static/dependencies/xlwings-logo.svg
        :target: https://www.xlwings.org/


Internal Dependencies
---------------------

GEMSEO source code includes software with compatible licenses that are listed below.

colormap
    CC0 1.0

`Fast Json schema <https://github.com/horejsek/python-fastjsonschema>`_
    MIT
    (Only used in Python 2.7, otherwise it is an external dependency)

`GenSON <https://github.com/wolverdude/GenSON>`_
    MIT

`prettytable <https://github.com/kxxoling/PTable>`_
    BSD 3-Clause

`pyXDSM <https://github.com/mdolab/pyXDSM>`_
    Apache v2.0

`SOMPY <https://github.com/sevamoo/SOMPY>`_
    Apache v2.0

`tqdm <https://github.com/noamraph/tqdm>`_
    MIT

    .. image:: /_static/dependencies/tqdm-logo.gif
        :target: https://github.com/noamraph/tqdm

`versioneer <https://github.com/python-versioneer/python-versioneer>`_
    CC0 1.0

`WhatsOpt-CLI <https://github.com/OneraHub/WhatsOpt-CLI>`_
    Apache v2.0

    .. image:: /_static/dependencies/whatsopt-logo.svg
        :target: https://github.com/OneraHub/WhatsOpt-CLI

External application
--------------------

Some external applications are used by GEMSEO,
but not linked with the application,
for documentation generation,
training or example purposes.

`Sphinx <http://www.sphinx-doc.org/>`_
    Sphinx

    .. image:: /_static/dependencies/sphinx-logo.png
        :target: http://www.sphinx-doc.org/

`Jupyter <https://jupyter.org/>`_
    BSD 3-Clause

    .. image:: /_static/dependencies/jupyter-logo.svg
        :target: https://jupyter.org/

Resources
---------

Some icons and fonts are used by GEMSEO or its documentation.

`gemseo/wrappers/icons <https://www.iconfinder.com/iconsets/basic-user-interface-elements>`_
    Creative Commons Attribution 3.0 Unported

`scikit-learn-modern theme <https://github.com/scikit-learn/scikit-learn>`_
    BSD 3-Clause
    (Using Bootstrap, a CSS framework, under MIT license)

    .. image:: /_static/dependencies/scikit-learn-logo.svg
        :target: https://scikit-learn.org/

`Font Awesome Free (fonts) <https://fontawesome.com/>`_
    SIL OFL 1.1

    .. image:: /_static/dependencies/font-awesome-logo.svg
        :target: https://fontawesome.com/

`Font Awesome Free (icons) <https://fontawesome.com/>`_
    CC BY 4.0 License

    .. image:: /_static/dependencies/font-awesome-logo.svg
        :target: https://fontawesome.com/
