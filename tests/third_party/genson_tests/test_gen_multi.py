# -*- coding: utf-8 -*-
# The MIT License (MIT)
#
# Copyright (c) 2014 Jon Wolverton github.com/wolverdude
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import, division, unicode_literals

import unittest

from gemseo.third_party.genson_generator import Schema

from . import base


class TestBasicTypes(base.SchemaTestCase):
    """"""

    def test_single_type(self):
        """"""
        s = Schema()
        s.add_object("bacon")
        s.add_object("egg")
        s.add_object("spam")
        self.assert_schema(s.to_dict(), {"type": "string"})

    def test_multi_type(self):
        """"""
        s = Schema()
        s.add_object("string")
        s.add_object(1.1)
        s.add_object(True)
        self.assert_schema(s.to_dict(), {"type": ["boolean", "number", "string"]})

    def test_redundant_integer_type(self):
        """"""
        s = Schema()
        s.add_object(1)
        s.add_object(1.1)
        self.assert_schema(s.to_dict(), {"type": "number"})


if __name__ == "__main__":
    unittest.main()
